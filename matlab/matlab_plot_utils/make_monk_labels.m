function [labels] = make_monk_labels()

labels = cell(18,1);
labels{1} = 'w';
labels{2} = 'o';
labels{3} = 'o';
labels{4} = 'o';
labels{5} = 'o';
labels{6} = 'o';
labels{7} = 'w';

labels{8} = 'y';
labels{9} = 'y';
labels{10} = 'y';
labels{11} = 'y';
labels{12} = 'y';
labels{13} = 'y';
labels{14} = 'y';
labels{15} = 'w';

labels{16} = '+';
labels{17} = '+';
labels{18} = '+';
